const DYNAMODB = require("aws-sdk/clients/dynamodb");
const SQS = require("aws-sdk/clients/sqs");

const dbClient = new DYNAMODB({
  region: "us-east-1",
});

const sqsClient = new SQS({
  region: "us-east-1",
});

const cardQueueURL = process.env.CREATE_CARD_QUEUE_URL;
const giftQueueURL = process.env.CREATE_GIFT_QUEUE_URL;
const clientsTable = process.env.CLIENTS_TABLE;

exports.handler = async (event) => {
  console.log('event', event);

  const body = JSON.parse(event.body);

  if (!body.dni || !body.name || !body.lastName || !body.birthdate) {
    return {
      statusCode: 400,
      body: "Incorrect properties",
    };
  }

  const age = _calculateAge(body.birthdate);

  if (age < 18 || age > 65) {
    return {
      statusCode: 400,
      body: "Client must be older than 18 years old and younger than 65 years old",
    };
  }

  const dbParams = {
    Item: {
      dni: {
        S: body.dni,
      },
      name: {
        S: body.name,
      },
      lastName: {
        S: body.lastName,
      },
      birthdate: {
        S: body.birthdate,
      },
    },
    TableName: clientsTable
  };

  const cardParams = {
    MessageBody: JSON.stringify(body),
    QueueUrl: cardQueueURL
  };  
  
  const giftParams = {
    MessageBody: JSON.stringify(body),
    QueueUrl: giftQueueURL
  };

  try {
    const dbResult = await dbClient.putItem(dbParams).promise();
    console.log('dbResult', dbResult);
    const cardResult = await sqsClient.sendMessage(cardParams).promise();
    console.log('cardResult', cardResult);
    const giftResult = await sqsClient.sendMessage(giftParams).promise();
    console.log('giftResult', giftResult);
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      body: error
    };
  }

  return {
    statusCode: 200,
    body: "Your request was processed successfully",
  };
};

/**
 * Reference: https://stackoverflow.com/questions/4060004/calculate-age-given-the-birth-date-in-the-format-yyyymmdd
 */ 
function _calculateAge(date) {
    const birthdate = new Date(date);
    const diff = Date.now() - birthdate.getTime();
    const ageDate = new Date(diff);
    return Math.abs(ageDate.getUTCFullYear() - 1970);
}