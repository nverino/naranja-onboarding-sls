## Serverless (Small Version) ##
Servicio serverless que crea un cliente en una tabla de DynamoDB, genera automáticamente una tarjeta de crédito y finalmente asigna un regalo para el cumpleaños.

**Nota:** Versión reducida que utiliza solamente API Gateway, colas SQS y Lambdas.

### Despliegue utilizando Serverless Framework ###
```
serverless deploy -v
```